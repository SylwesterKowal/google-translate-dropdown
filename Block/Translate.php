<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoogleTranslateDropdown\Block;

use Magento\Store\Model\ScopeInterface;

class Translate extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context   $context,
        array                                              $data = [],
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function display()
    {
        //Your block code
        return __('Hello Developer! This how to get the storename: %1 and this is the way to build a url: %2', $this->_storeManager->getStore()->getName(), $this->getUrl('contacts'));
    }

    public function getEnabled($storeId = 0)
    {
        return $this->getConfigValue('googletranslate/settings/enable', $storeId);
    }

    public function getLanguages($storeId = 0)
    {
        if ($languages = $this->getConfigValue('googletranslate/settings/languages', $storeId)) {
            return $languages;
        } else {
            return 'en,es,de,fr';
        }
    }


    public function getConfigValue($field, $storeId = 0)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

}

