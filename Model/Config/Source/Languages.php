<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\GoogleTranslateDropdown\Model\Config\Source;

class Languages implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
            ['value' => 'en', 'label' => __('English')],
            ['value' => 'fr', 'label' => __('French')],
            ['value' => 'de', 'label' => __('German')],
            ['value' => 'ru', 'label' => __('Russian')],
            ['value' => 'es', 'label' => __('Spanish')],
            ['value' => 'af', 'label' => __('Afrikaans')],
            ['value' => 'sq', 'label' => __('Albanian')],
            ['value' => 'am', 'label' => __('Amharic')],
            ['value' => 'ar', 'label' => __('Arabic')],
            ['value' => 'hy', 'label' => __('Armenian')],
            ['value' => 'az', 'label' => __('Azerbaijani')],
            ['value' => 'eu', 'label' => __('Basque')],
            ['value' => 'be', 'label' => __('Belarusian')],
            ['value' => 'bn', 'label' => __('Bengali')],
            ['value' => 'bs', 'label' => __('Bosnian')],
            ['value' => 'bg', 'label' => __('Bulgarian')],
            ['value' => 'ca', 'label' => __('Catalan')],
            ['value' => 'ceb', 'label' => __('Cebuano')],
            ['value' => 'zh', 'label' => __('Chinese')],
            ['value' => 'co', 'label' => __('Corsican')],
            ['value' => 'hr', 'label' => __('Croatian')],
            ['value' => 'cs', 'label' => __('Czech')],
            ['value' => 'da', 'label' => __('Danish')],
            ['value' => 'nl', 'label' => __('Dutch')],
            ['value' => 'eo', 'label' => __('Esperanto')],
            ['value' => 'et', 'label' => __('Estonian')],
            ['value' => 'ft', 'label' => __('Finnish')],
            ['value' => 'fy', 'label' => __('Frisian')],
            ['value' => 'gl', 'label' => __('Galician')],
            ['value' => 'ka', 'label' => __('Georgian')],
            ['value' => 'gu', 'label' => __('Gujarati')],
            ['value' => 'ht', 'label' => __('Haitian Creole')],
            ['value' => 'ha', 'label' => __('Hausa')],
            ['value' => 'haw', 'label' => __('Hawaiian')],
            ['value' => 'he', 'label' => __('Hebrew')],
            ['value' => 'hi', 'label' => __('Hindi')],
            ['value' => 'hmn', 'label' => __('Hmong')],
            ['value' => 'hu', 'label' => __('Hungarian')],
            ['value' => 'is', 'label' => __('Icelandic')],
            ['value' => 'ig', 'label' => __('Igbo')],
            ['value' => 'id', 'label' => __('Indonesian')],
            ['value' => 'ga', 'label' => __('Irish')],
            ['value' => 'it', 'label' => __('Italian')],
            ['value' => 'ja', 'label' => __('Japanese')],
            ['value' => 'jv', 'label' => __('Javanese')],
            ['value' => 'kn', 'label' => __('Kannada')],
            ['value' => 'kk', 'label' => __('Kazakh')],
            ['value' => 'km', 'label' => __('Khmer')],
            ['value' => 'rw', 'label' => __('Kinyarwanda')],
            ['value' => 'ko', 'label' => __('Korean')],
            ['value' => 'ku', 'label' => __('Kurdish')],
            ['value' => 'ky', 'label' => __('Kyrgyz')],
            ['value' => 'lo', 'label' => __('Lao')],
            ['value' => 'lv', 'label' => __('Latvian')],
            ['value' => 'lt', 'label' => __('Lithuanian')],
            ['value' => 'lb', 'label' => __('Luxembourgish')],
            ['value' => 'mk', 'label' => __('Macedonian')],
            ['value' => 'mg', 'label' => __('Malagasy')],
            ['value' => 'ms', 'label' => __('Malay')],
            ['value' => 'ml', 'label' => __('Malayalam')],
            ['value' => 'mi', 'label' => __('Maori')],
            ['value' => 'mr', 'label' => __('Marathi')],
            ['value' => 'mn', 'label' => __('Mongolian')],
            ['value' => 'my', 'label' => __('Myanmar (Burmese)')],
            ['value' => 'ne', 'label' => __('Nepali')],
            ['value' => 'no', 'label' => __('Norwegian')],
            ['value' => 'ny', 'label' => __('Nyanja (Chichewa)')],
            ['value' => 'or', 'label' => __('Odia (Oriya)')],
            ['value' => 'ps', 'label' => __('Pashto')],
            ['value' => 'fa', 'label' => __('Persian')],
            ['value' => 'pl', 'label' => __('Polish')],
            ['value' => 'pt', 'label' => __('Portuguese (Portugal, Brazil)')],
            ['value' => 'pa', 'label' => __('Punjabi')],
            ['value' => 'ro', 'label' => __('Romanian')],
            ['value' => 'sm', 'label' => __('Samoan')],
            ['value' => 'gd', 'label' => __('Scots Gaelic')],
            ['value' => 'sr', 'label' => __('Serbian')],
            ['value' => 'st', 'label' => __('Sesotho')],
            ['value' => 'sn', 'label' => __('Shona')],
            ['value' => 'sd', 'label' => __('Sindhi')],
            ['value' => 'si', 'label' => __('Sinhala (Sinhalese)')],
            ['value' => 'sk', 'label' => __('Slovak')],
            ['value' => 'sl', 'label' => __('Slovenian')],
            ['value' => 'so', 'label' => __('Somali')],
            ['value' => 'su', 'label' => __('Sundanese')],
            ['value' => 'sw', 'label' => __('Swahili')],
            ['value' => 'sv', 'label' => __('Swedish')],
            ['value' => 'tl', 'label' => __('Tagalog (Filipino)')],
            ['value' => 'tg', 'label' => __('Tajik')],
            ['value' => 'ta', 'label' => __('Tamil')],
            ['value' => 'tt', 'label' => __('Tatar')],
            ['value' => 'te', 'label' => __('Telugu')],
            ['value' => 'th', 'label' => __('Thai')],
            ['value' => 'tr', 'label' => __('Turkish')],
            ['value' => 'tk', 'label' => __('Turkmen')],
            ['value' => 'uk', 'label' => __('Ukrainian')],
            ['value' => 'ur', 'label' => __('Urdu')],
            ['value' => 'ug', 'label' => __('Uyghur')],
            ['value' => 'uz', 'label' => __('Uzbek')],
            ['value' => 'vi', 'label' => __('Vietnamese')],
            ['value' => 'cy', 'label' => __('Welsh')],
            ['value' => 'xh', 'label' => __('Xhosa')],
            ['value' => 'yi', 'label' => __('Yiddish')],
            ['value' => 'yo', 'label' => __('Yoruba')],
            ['value' => 'zu', 'label' => __('Zulu')],
            ];
    }

    public function toArray()
    {
        $_array = [];
        $arr = $this->toOptionArray();
        foreach ($arr as $a){
            $_array[$a['value']] = $a['label'];
        }
        return $_array;
    }
}

